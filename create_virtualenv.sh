#! /bin/bash
set -e

if which virtualenv; then
  virtualenv --python=python3 venv
elif which pyvenv; then
  pyvenv venv
else
  exit 1
fi

source venv/bin/activate
pip install -r requirements.txt
