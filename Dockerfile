FROM python:3.6

ADD app.py requirements.txt ./
RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["./app.py"]
